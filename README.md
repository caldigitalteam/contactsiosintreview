## IOS Programming Task

In order to be considered for the IOS position, you must complete the following steps. 

*Note: Try to finish this test in two hours.*

### Prerequisites

- Knowledge of IOS and Xcode.
- Knowledge of Swift.

## Task

Create an IOS app that accomplishes the following:

- Create a Contacts app
- Enable add/update/delete contact
- Support image/phone/email 
- Support Drill to specifit contact viewer
- Support dialing (on phone number tap) in contact viewer

## Judging

- Code quality: testability, encapsulation, design patterns, comments, readable code, clean code, etc.
- UI/UX: This is the place for you to show your creativity.

## Advice and Bonus

- You are encourage to use libraries to help you produce better code.	
	
*Note: Cheating or copying code will lead to disqualifying*